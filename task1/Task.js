
const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'}
];

const isTimeFrontendTask = () => {
    return tasks.filter((task) => task.category === 'Frontend').reduce((acc, task) => task.timeSpent + acc, 0);

};

console.log(isTimeFrontendTask());

const isTimeBugTask = () => {
    return tasks.filter((task) => task.type === 'bug').reduce((acc, task) =>task.timeSpent + acc, 0);
};

console.log(isTimeBugTask());

const isQualityTaskUi = () => {
    return tasks.filter((task) => task.title.includes('UI')).length
};
console.log(isQualityTaskUi());

const getQualityTasks = () => {
    return tasks.reduce((acc, task) => {

        acc[task.category] ? acc[task.category]++ : acc[task.category] = 1;
        return acc;
    }, {})
};
console.log(getQualityTasks());

const isTasksFourHour = () => {
    return tasks.filter((task) => task.timeSpent > 4).map((task) => ({title: task.title, category: task.category}));

};
 console.log(isTasksFourHour());

import React, { Component } from 'react';

import SpendMoney from "./container/SpendMoney/SpendMoney";

class App extends Component {
  render() {
    return (
      <SpendMoney />
    );
  }
}

export default App;

import React from 'react';
import  './SpendItemControl.css';

const SpendItemControl =(props) => {
    return (
        <div>

            <input className="inputText" type="text" placeholder="Item name" value={props.text} onChange={props.textChange} />
            <input className="inputCost" type="number" placeholder="Cost" value={props.cost} onChange={props.changeCost} />KGS

            <button onClick ={props.click} className="SpendItemControl-btn">
                Add
            </button>
        </div>
    );

};
 export default  SpendItemControl
import React from 'react';
import './SpendPreview.css';

const SpendPreview = props => {
    let totalSpent = 0;



    return (
        <div className="SpendPreview">
            {props.items.map((item, i) => {
                totalSpent+=parseInt(item.cost);
                return <p key={item.id}>{item.text} | {item.cost}<span onClick={() => props.remove(item.id)}>X</span></p>
            })}
            <p><strong>Total spent: {totalSpent}</strong></p>
        </div>
    )
};

export default SpendPreview;
import React, {Component} from 'react';
import SpendPreview from '../../components/SpendPreview/SpendPreview';
import SpendItemControl from '../../components/SpendItemConrol/SpendItemControl';

import './SpendMoney.css';



class SpendMoney extends Component {
    state = {
        items: [],
        text: '',
        cost: '',
        totalSpent: 0
    };

    // addItemToOrder = type => {
    //     const itemIndex = this.state.order.findIndex(orderItem => orderItem.type === type);
    //     const orderCopy = [...this.state.order];
    //
    //     if (itemIndex !== -1) {
    //         const itemCopy = {...orderCopy[itemIndex]};
    //         itemCopy.amount++;
    //         orderCopy[itemIndex] = itemCopy;
    //     } else {
    //         orderCopy.push({type, amount: 1});
    //     }
    //
    //     this.setState({order: orderCopy});
    // };

    handleTextChange = (e) => {
        this.setState({text: e.target.value});
    };

    handleCostChange = e => this.setState({cost: e.target.value});

    addItems = (e) => {
        e.preventDefault();
       const items = [...this.state.items];
       items.push({
            text: this.state.text,
            cost: this.state.cost,
           id: Date.now()
       });
        this.setState({items});
    };
    removeItems = (id) => {
       this.setState({items: this.state.items.filter((item, index) => item.id !==id)})

    };




    render() {
        return (
            <div className="SpendMoney">

                <SpendItemControl
                                  text={this.state.text}
                                  cost={this.state.cost}
                                  changeCost = {this.handleCostChange}
                                  textChange={this.handleTextChange}
                                  click={this.addItems}
                />
                <SpendPreview items={this.state.items} remove={this.removeItems}/>
            </div>
        );
    }
}

export default SpendMoney;